#!/bin/sh
fast_sync(){
	unison -batch -repeat watch -times -silent fast
}

slow_sync(){
	unison -batch -copyonconflict -times -terse pi
}

remote_sync(){
	unison -batch -copyonconflict -times -auto pir
}

if slow_sync; then
		fast_sync
else
	remote_sync
fi
