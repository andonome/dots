#!/bin/sh
fast_sync(){
	unison -batch -repeat watch -times -silent fast
}

slow_sync(){
	unison -batch -copyonconflict -times -terse pi
}

remote_sync(){
	t=5
	for x in $(seq 1..20); do
			unison -batch -copyonconflict -times -auto pir
			sleep "$t"m
			t=$(( t + 5 ))
	done
}

if slow_sync; then
		fast_sync
else
	remote_sync
fi
