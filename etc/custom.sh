export VISUAL=vim
export EDITOR=vim
export TERM=xterm
export ARC=$(date +%Y/%m)
export PAGER="bat --style=plain"
export MANPAGER="bat --style=plain"

export XDG_CACHE_HOME=$HOME/.cache
export XDG_CONFIG_HOME=$HOME/.config
export XDG_DATA_HOME=$HOME/.local/share
export XDG_STATE_HOME=$HOME/.local/state

export GOPATH=/tmp
export GOBIN=/tmp
