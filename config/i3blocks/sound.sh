#!/bin/sh

SOUND_LEVEL="$(amixer get Master | grep -Pm1 -o ' \[\K[0-9]+')"
VOLUME_MUTE="🔇"
VOLUME_LOW="🔈"
VOLUME_MID="🔉"
VOLUME_HIGH="🔊"

ICON=$VOLUME_MUTE

if [ "$SOUND_LEVEL" = "" ]; then
	ICON=$VOLUME_MUTE
elif [ "$SOUND_LEVEL" -lt 34 ]; then
	ICON="$VOLUME_LOW"
elif [ "$SOUND_LEVEL" -lt 67 ]; then
	ICON="$VOLUME_MID"
else
	ICON="$VOLUME_HIGH"
fi

echo "$ICON $SOUND_LEVEL%"
