#!/bin/sh

disk=$(df | grep /$ | awk '{print $5}' | sed s/%//)

ram=$(free | grep Mem | awk '{print $3/$2}' | cut -d'.' -f2 | head -c2)

if [ "$disk" -gt "$ram" ]; then
	output="󰋊 $disk%"
else
	output=" $ram%"
fi

pgrep -u "$USER" gpg-agent >/dev/null || output='⛔'

echo "$output"
