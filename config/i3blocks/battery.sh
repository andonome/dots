#!/bin/sh

location=/sys/class/power_supply/
battery=BAT1

percent=$(cat $location"$battery"/capacity)
status=$(cat $location"$battery"/status)

if [ $(grep -q Discharging $location$battery/status  && echo y) ]; then
	symbol=🔋
	[ "$percent" -lt 7 ] && symbol=🪫 && msg='BATTERY LOW'
	[ "$percent" -lt 4 ] && amixer -q sset Master 90% && pkill -RTMIN+1 \
		i3blocks && \
		play /usr/share/backgrounds/drowning.mp3
		sleep 2m
else
	symbol=🔌 && msg=''
fi

echo $symbol $percent% $msg
