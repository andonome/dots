#!/bin/bash
#

CACHE=~/.cache/i3blocks/notification

# Ensure the cache exists
mkdir -p `dirname $CACHE`
touch $CACHE

if env | grep -q BLOCK_
then # called by i3blocks

  # clear notification on click
  test $BLOCK_BUTTON -ne 0 && cp /dev/null $CACHE

  # source the notification
  . $CACHE

  FULL_TEXT="$SUMMARY $BODY"
  SHORT_TEXT="$SUMMARY"

  case $URGENCY in
    LOW)
      COLOR=#FFFFFF
      CODE=0
      ;;
    NORMAL)
      COLOR=#AAAAAA
      CODE=0
      ;;
    CRITICAL)
      COLOR=#FF0000
      CODE=33
      ;;
    *)
      # unknown urgency, certainly empty notification
      exit 0
      ;;
  esac

  # Output the status block
  echo $FULL_TEXT
  echo $SHORT_TEXT
  echo $COLOR
  exit $CODE

else # called by dunst

  # store the notification
  # some notifications come up with - can&apos;t - instead of "can't"
  cat << dunst > $CACHE
APPNAME="$1"
SUMMARY="$(echo $2 | sed "s/&apos;/'/g")"
BODY="$(echo $3 | sed "s/&apos;/'/g")"
ICON="$4"
URGENCY="$5"
dunst

  # notify
  /usr/share/backgrounds/ring.mp3
  # signal i3blocks that there is a new notification
  pkill -RTMIN+12 i3blocks
  # remove the notification after a minute
  rm -f $CACHE-tracker-*
  TASKNO=$(( RANDOM % 1000 ))
  cp $CACHE $CACHE-tracker-$TASKNO
  sleep 2m && [ -e $CACHE-tracker-$TASKNO ] && cp /dev/null $CACHE && pkill -RTMIN+12 i3blocks
  exit

fi

