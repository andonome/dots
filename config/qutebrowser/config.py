# Uncomment this to still load settings configured via autoconfig.yml
config.load_autoconfig(False)
c.url.default_page = "https://www.jwz.org/webcollage/collage.html"
c.url.start_pages = "file:///usr/share/backgrounds/nimbus.png"

# Background color for hints. Note that you can use a `rgba(...)` value
# for transparency.
# Type: QssColor
#c.colors.hints.bg = 'black'
c.tabs.title.format = "{index}:{current_title}"

# Font color for hints.
# Type: QssColor
#c.colors.hints.fg = 'white'

# Background color for prompts.
# Type: QssColor
#c.colors.prompts.bg = '#000000'

# Background color for webpages if unset (or empty to use the theme's
# color).
# Type: QtColor
c.content.webgl = False

c.qt.workarounds.disable_hangouts_extension = True

# List of user stylesheet filenames to use.
# Type: List of File, or File
c.content.user_stylesheets = "~/.config/qutebrowser/css/solarized-everything-css/css/solarized-dark/solarized-dark-reddit.com.css" 
# Padding (in pixels) around text for tabs.
# Type: Padding
c.tabs.padding = {'bottom': 5, 'left': 5, 'right': 5, 'top': 5}
c.tabs.show = 'multiple'

c.downloads.location.directory = '~/dl'

c.spellcheck.languages = ["en-GB"]

# Position of the tab bar.
# Type: Position
# Valid values:
#   - top
#   - bottom
#   - left
#   - right
c.tabs.position = 'bottom'
c.colors.webpage.bg = 'black'
c.colors.webpage.darkmode.enabled = True

# Bindings for normal mode
config.bind(',d', 'config-cycle colors.webpage.darkmode.enabled True False')
config.bind(',n', 'config-cycle content.user_stylesheets ~/.config/qutebrowser/css/solarized-everything-css/css/solarized-dark/solarized-dark-reddit.com.css ""')
config.bind(',b', 'config-cycle colors.webpage.bg white black')
config.bind(',r', 'spawn --userscript /usr/share/qutebrowser/userscripts/readability')
config.bind('M', 'hint links spawn nohup mpv --cache=yes --demuxer-max-bytes=300M --demuxer-max-back-bytes=100M -ytdl-format="bv[ext=mp4]+ba/b" {hint-url}')

config.bind('<Ctrl-J>', 'scroll-page 0 0.5')
config.bind('<Ctrl-K>', 'scroll-page 0 -0.5')
config.bind('<Ctrl-e>', 'edit-text')
config.bind('<Ctrl-u>', 'edit-url')

# Editors

c.prompt.filebrowser=True

c.editor.command=["urxvt", "-e", "vim", "{file}", "-c", "normal {line}G{column0}l"]
c.fileselect.single_file.command=["urxvt", "-e", "ranger", "--choosefile={}"]
c.fileselect.folder.command=["urxvt", "-e", "ranger", "--choosedir={}"]
c.fileselect.multiple_files.command=["urxvt", "-e", "ranger", "--choosefiles={}"]

c.content.pdfjs = True

# Search engines
c.url.searchengines = {
    "DEFAULT": "https://duckduckgo.com/?q={}",
    "v":"https://sepiasearch.org/search?search={}&sort=-match&page=1",
    "p":"https://thepiratebay.org/search.php?q={}",
    "o":"https://3g2upl4pq6kufc4m.onion/?q={}","l":"https://liberch.com/?q={}",
    "wa": "https://wiki.archlinux.org/?search={}",
    "i": "https://duckduckgo.com?q={}&iax=images&ia=images",
    "g": "https://www.google.com/search?hl=en&q={}",
    "n": "https://inv.nadeko.net/results?search_query={}",
    "y": "https://www.youtube.com/results?search_query={}",
    "q": "https://www.qwant.com/?q={}",
    "qi": "https://www.qwant.com/?q={}&t=images",
    "r": "https://duckduckgo.com/?q=site%3Areddit.com+{}&ia=web",
    "b": "https://bandcamp.com/search?q={}&item_type=",
    "w":"https://en.wikipedia.org/w/index.php?search={}"
    }

