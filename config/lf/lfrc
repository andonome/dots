set ratios 1:2:3
# interpreter for shell commands (needs to be POSIX compatible)
set shell bash

# set the previewer and cleaner scriptps for image previews

set previewer ~/.config/lf/preview
set cleaner ~/.config/lf/cleaner

# show icons
# these are defined in the icons file

set icons

# set '-eu' options for shell commands
# These options are used to have safer shell commands. Option '-e' is used to
# exit on error and option '-u' is used to give error for unset variables.
# Option '-f' disables pathname expansion which can be useful when $f, $fs, and
# $fx variables contain names with '*' or '?' characters. However, this option
# is used selectively within individual commands as it can be limiting at
# times.
set shellopts '-eu'

# set internal field separator (IFS) to "\n" for shell commands
# This is useful to automatically split file names in $fs and $fx properly
# since default file separator used in these variables (i.e. 'filesep' option)
# is newline. You need to consider the values of these options and create your
# commands accordingly.
set ifs "\n"

# leave some space at the top and the bottom of the screen
set scrolloff 10

# use enter for shell commands
map <enter> shell-wait
map <Shift>+<enter> shell-async

# execute current file (must be executable)
map x $$f
map X !$f

# dedicated key for file opener actions
map o &xdg-open $f

# watch thing later

map W %for ep in $fx; do ls "$ep" | sed "s/.part$//" >> ~/.local/watchlist; done

# define a custom 'open' command
# This command is called when current file is not a directory. You may want to
# use either file extensions and/or mime types here. Below uses an editor for
# text files and a file opener for the rest.
cmd open ${{
    case $(file --mime-type $f -b) in
        application/x-sc) sc-im $fx;;
        text/html) w3m $fx;;
        text/*) $EDITOR $fx;;
        video/*) nohup mpv $fx --really-quiet >/dev/null &;;
        *) nohup $OPENER $fx >/dev/null &;;
    esac
}}

# drag-and-drop GUI box
# you need to make 'dragon' link to the right program (on arch: dragon-drop)

map T %dragon -a -x $fx

# rename current file without overwrite
cmd rename %echo 'name: ' ; read name ; extension="${f##*.}" && newname="$name.$extension"; [ "$f" = "$extension" ] && newname="$name"; [ ! -e "$newname" ] && mv "$f" "$newname" || echo file exists
map r push :rename<enter>

# make sure trash folder exists
%mkdir -p ~/.local/share/Trash/files/

# move current file or selected files to trash folder
# (also see 'man mv' for backup/overwrite options)
cmd trash %set -f; gio trash $fx
map b trash $fx

# delete current file or selected files (prompting)
cmd delete ${{
    set -f
    printf "%s\n" "$fx"
    printf "delete?[y/n]"
    read ans
    [ $ans = "y" ] && rm -rf $fx
}}

# extract the current file with the right command
# (xkcd link: https://xkcd.com/1168/)
cmd extract ${{
    set -f
    case $f in
        *.tar.bz|*.tar.bz2|*.tbz|*.tbz2) tar xjvf $f;;
        *.tar.gz|*.tgz) tar xzvf $f;;
        *.tar.xz|*.txz) tar xJvf $f;;
        *.zip) unzip $f;;
        *.rar) unrar x $f;;
        *.7z) 7z x $f;;
    esac
}}

# compress current file or selected files with tar and gunzip
cmd tar ${{
    set -f
    mkdir $1
    cp -r $fx $1
    tar czf $1.tar.gz $1
    rm -rf $1
}}

# compress current file or selected files with zip
cmd zip ${{
    set -f
    mkdir $1
    cp -r $fx $1
    zip -r $1.zip $1
    rm -rf $1
}}

source ~/.map

map g/ cd /
map gM cd /mnt
map gt cd /tmp
