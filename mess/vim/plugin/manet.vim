let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 0
let g:netrw_preview = 1
let g:netrw_altv = 1
let g:netrw_winsize = 25
let g:netrw_list_hide=netrw_gitignore#Hide() .. '.*\.swp$,\.git'

map ,f :22Lexplore<Enter>
