#!/bin/bash

mpc single off

mpc clear

mpc load night

mpc shuffle

mpc volume 0

mpc play

for i in {1..60}
do
	mpc volume +1
	sleep 30
done

for i in {1..60}
do
	mpc volume -1
	sleep 30
done

sleep 15m

mpc pause

mpc volume 60
