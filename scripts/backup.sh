#!/bin/sh

HOSTNAME=$(cat /etc/hostname)

cd ~

tar czf /mnt/dungeon/backups/$HOSTNAME/backup-$(date +%d).tgz \
Mail \
.gnupg \
rec \
.timewarrior \
.local/share/calcurse \
.local/share/task \
.local/share/khard \
.local/share/khal \
.local/share/newsraft \
.config/.bombadillo.ini \
.config/lagrange \
.config/amfora \
.local/share/amfora \
.config/aerc
