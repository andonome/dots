#!/bin/bash
s=0

mpc single off

mpc clear

mpc load morning

mpc shuffle

mpc volume 0

mpc play

for i in {1..99}
do
	mpc volume +1
	sleep 15
done

sleep 15m

[ "$(mpc | grep playing -c)" -eq 0 ] && (( s++ ))

ping -c 1 ratking && (( s++ ))

[ $s -gt 0 ] && mplayer ~/Music/song.mp3 && mpc volume 100 && echo 'Wake failure' > ~/wakeup.log

