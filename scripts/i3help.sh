#!/bin/sh

helpMsg='
# Key Changes

- Capslock and Escape are swapped.

# Standard Commands

- **Go to a desktop**: Windows + number
    * (e.g. Windows + 3)
- **Open a program**: Windows + d
    * (e.g. "Firefox")
- **Close a program**: Windows + q
- **Find a program**: Windows + z
- **Volume up/ down**: standard volume keys
    * Or Shift + F1/ F2

# Movement

- **Fullscreen**: Windows + f
- **Move program**: Windows + Shift + number
    * (e.g. "Windows + Shift + 2" will move the program to desktop 3)

# Standard Questions:

- *Why is your keyboard weird?*
    * Because Hungarians are weird.
- *Why is your computer weird?*
    * Why is your face weird?
'

echo "$helpMsg" | lowdown -stms  | pdfroff -itk -mspdf | zathura -
