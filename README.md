These are my dots.
There are many like them, but these ones are mine.

Don't run these scripts on a real machine - they might mess up the whole OS.

## Live Installation

Install from an Arch or Void Linux setup by `curl`ing or `gemget`ing the partition script:

> gemget splint.rs/share/partition.sh

### Arch Linux Arm

The `Makefile` under `setup/archpi` will set up an SD card to run Arch Linux Arm.

> sudo make -e DISK=sdx

## Setup

For an existing VM:

> git clone ssh://soft.dmz.rs/dots ~/.dots

> vi ~/.dots/vars

Then add your Gitlab username to pull in your ssh keys, and the rest, and replace all dot-files:

> cd ~/.dots && make

The setup is mostly idempotent.
