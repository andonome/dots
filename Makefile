output: link messlinks configlinks defaults scripts cpu_usage $(UNISON_TARGETS)

HOSTNAME := $(shell cat /etc/hostname)

scripts :: $(HOME)/.scripts
$(HOME)/.scripts:
	@ln -s $(PWD)/scripts/ $(HOME)/.scripts

HOMEFILES := $(shell ls files)
DOTFILES := $(addprefix $(HOME)/.,$(HOMEFILES))

MESSDIRS := $(shell ls mess)
HOMEDIRS := $(addprefix $(HOME)/.,$(MESSDIRS))

DOTCONFIGS := $(shell ls config)
CONFIGDIRS := $(addprefix $(HOME)/.config/,$(DOTCONFIGS))

.PHONEY: link unlink messlinks backup configlinks defaults cpu_usage

defaults:
	@./setup/defaults.sh

UNISON = $(shell ls mess/unison/)
UNISON_TARGETS = $(addprefix $(HOME)/.unison/,$(UNISON))

$(HOME)/.unison/%.prf: mess/unison/%.prf
	cp -u $< $@

messlinks: messdirs $(HOME)/.w3m/ddg.html $(HOME)/.gnupg/gpg-agent.conf vim
$(HOME)/.w3m/ddg.html:
	@ln -s $(PWD)/mess/w3m/ddg.html $(HOME)/.w3m/ddg.html
	@ln -s $(PWD)/mess/w3m/keymap $(HOME)/.w3m/keymap
$(HOME)/.gnupg/gpg-agent.conf:
	@ln -s $(PWD)/mess/gnupg/gpg-agent.conf $(HOME)/.gnupg/
.PHONEY: vim
vim: $(HOME)/.vim/skel $(HOME)/.vim/plugin $(HOME)/.vim/doc
$(HOME)/.vim/skel:
	@ln -s $(PWD)/mess/vim/skel $(HOME)/.vim/
$(HOME)/.vim/plugin: mess/vim/plugin
	@ln -sf $(PWD)/mess/vim/plugin $(HOME)/.vim/
	@ln -sf $(PWD)/mess/vim/ftplugin $(HOME)/.vim/
$(HOME)/.vim/doc: $(HOME)/.vim/plugin
	@cp -r mess/vim/doc/ $(HOME)/.vim/
	@vim -c ":helptags $(HOME)/.vim/doc" -c q

link: | $(HOMEFILES)

$(HOMEFILES):
	@ln -sf $(PWD)/files/$@ $(HOME)/.$@

$(HOMEDIRS):
	@mkdir -pm700 $@

messdirs: | $(HOMEDIRS)

configlinks: | $(CONFIGDIRS)
$(CONFIGDIRS):| $(DOTCONFIGS)
$(DOTCONFIGS):
	@find config/ -type d -exec mkdir -pm700 ~/.'{}' ';'
	@find config/ -type f -exec ln -sf $(PWD)/'{}' $(HOME)/.'{}' ';'

cpu_usage: $(HOME)/.config/i3blocks/cpu_usage.out configlinks
$(HOME)/.config/i3blocks/cpu_usage.out:
	gcc config/i3blocks/cpu_usage.c -o $(HOME)/.config/i3blocks/cpu_usage.out

unlink:
	@echo "Unlinking dotfiles"
	@for f in $(DOTFILES); do if [ -h $$f ]; then rm -i $$f; fi; done

backups:
	mkdir -p backups

backup: /var/spool/cron/$(USER)
	cp /var/spool/cron/$(USER) etc/cron/user
