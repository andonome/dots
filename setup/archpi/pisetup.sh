#!/bin/sh

## This setup file for archlinux arm prepares pacman with all the right keys, then makes sure networking is working.
## If you ignore the networking, then the network might not work after your first reboot!

set -e
set -x

pacman-key --init
pacman-key --populate archlinuxarm
pacman -Syu
gpgconf --kill all
pacman -S networkmanager arch-install-scripts base-devel git curl ffmpeg pulseaudio alsa-plugins
genfstab / > /etc/fstab

systemctl enable NetworkManager

echo making swap file

## This sets up the swapfile and adds it to /etc/fstab.

cd /var/cache/

dd if=/dev/zero of=swapfile bs=1K count=4M status=progress

chmod 600 swapfile

mkswap swapfile

echo "/var/cache/swapfile none swap sw 0 0" >> /etc/fstab

swapon -va

## Uncomment this line to use all my dots.
## You'll need to change the stuff in the vars file, because it pulls in my ssh keys (so I get access to the machine).
## You'll probably need to change other stuff.

# git clone https://gitlab.com/andonome/dots
