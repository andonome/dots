#!/bin/sh

command -v xdg-mime >/dev/null || exit 0

# default applications
xdg-mime default vim.desktop text/plain
xdg-mime default vim.desktop text/x-tex
xdg-mime default sc-im.desktop application/x-sc
xdg-mime default mpv.desktop audio/mpeg
xdg-mime default mpv.desktop audio/*
xdg-mime default mpv.desktop audio/x-m4a
xdg-mime default mpv.desktop audio/x-wav
xdg-mime default mpv.desktop video/x-matroska
xdg-mime default mpv.desktop image/gif
xdg-mime default mpv.desktop video/*
xdg-mime default mpv.desktop video/webm
xdg-mime default mpv.desktop video/quicktime
xdg-mime default mpv.desktop video/mp4
xdg-mime default org.fontforge.FontForge.desktop font/sfnt
xdg-mime default org.gnome.font-viewer.desktop application/vnd.ms-opentype
xdg-mime default org.gnome.font-viewer.desktop font/sfnt
xdg-mime default org.inkscape.Inkscape.desktop image/svg+xml
xdg-mime default org.pwmt.zathura.desktop application/pdf
xdg-mime default sxiv.desktop image/jpeg
xdg-mime default sxiv.desktop image/jpg
xdg-mime default sxiv.desktop image/webp
xdg-mime default sxiv.desktop image/png
xdg-settings set default-web-browser org.qutebrowser.desktop
xdg-mime default amfora.desktop x-scheme-handler/gemini
xdg-mime default aerc.desktop x-scheme-handler/mailto
